package org.cdi.jdbc.taca.foot.program.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * Match class is used to create instance of matches imported by the user in the
 * application, the instances will then be exported in the database.
 * 
 * @author Choteau
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Match {

	/**
	 * Name of the home team
	 */
	private String eqHome;

	/**
	 * Name of the visitor team
	 */
	private String eqVis;

	/**
	 * Id the the day
	 */
	private Integer nojour;

	/**
	 * Number of goals scored by the home team
	 */
	private Integer butHome;

	/**
	 * Number of goals scored by teh visitor team
	 */
	private Integer butVis;
}
