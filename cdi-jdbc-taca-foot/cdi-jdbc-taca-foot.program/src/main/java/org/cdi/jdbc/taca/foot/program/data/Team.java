package org.cdi.jdbc.taca.foot.program.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Class representing a team.
 * 
 * @author Choteau
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Team {
	/**
	 * The team's ID.
	 */
	private String noequ;

	/**
	 * The team's name.
	 */
	private String nomEqu;

	/**
	 * The address of team's stadium's.
	 */
	private String address;

	/**
	 * The team's date of creation.
	 */
	private String dateCreation;
}
