package org.cdi.jdbc.taca.foot.program.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Class saving the results for a determined team and season.
 * 
 * @author Choteau
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Result {

	/**
	 * Id of the saison.
	 */
	private int noSaison;

	/**
	 * Name of the team which results are saved.
	 */
	private String teamId;

	/**
	 * Number of total points.
	 */
	private int points;

	/**
	 * Number of matches won.
	 */
	private int matchWon;

	/**
	 * Number of null matches.
	 */
	private int matchNull;

	/**
	 * Number of matches lost.
	 */
	private int matchLost;

	/**
	 * Number of goals scored when at home.
	 */
	private int goalsScoredHome;

	/**
	 * Number of goals scored when being visitors.
	 */
	private int goalsScoredExt;

	/**
	 * Number of goals taken when at home.
	 */
	private int goalsTakenHome;

	/**
	 * Number of goals taken when being visitors.
	 */
	private int goalsTakenExt;
}
