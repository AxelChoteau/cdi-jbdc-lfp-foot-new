package org.cdi.jdbc.taca.foot.program.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Class reprensenting a season.
 * 
 * @author Choteau
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Season {

	/**
	 * Id of the season.
	 */
	private int noseason;

	/**
	 * Is the season over or running?
	 */
	private boolean isOver;
}
