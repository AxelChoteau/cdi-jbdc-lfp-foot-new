package org.cdi.jdbc.taca.foot.api;

public interface DAO<T> {
	public abstract boolean insert(T obj);

	public abstract boolean update(T obj);

	public abstract T selectAll();

	public default T selectClassement(String... id) {
		return null;
	}

	public default T selectStatsEquipe(String equipeID) {
		return null;
	}

	public default T selectStatsBestAttAllTime() {
		return null;
	}

	public default T selectStatsBestAttHome(String nosaison) {
		return null;
	}

	public default T selectStatsBestAttExt(String nosaison) {
		return null;
	}

	public default T selectStatsWorstAttAllTime() {
		return null;
	}

	public default T selectStatsWorstAttHome(String nosaison) {
		return null;
	}

	public default T selectStatsWorstAttExt(String nosaison) {
		return null;
	}

	public default T selectStatsBestDefAllTime() {
		return null;
	}

	public default T selectStatsBestDefHome(String nosaison) {
		return null;
	}

	public default T selectStatsBestDefExt(String nosaison) {
		return null;
	}

	public default T selectStatsWorstDefAllTime() {
		return null;
	}

	public default T selectStatsWorstDefHome(String nosaison) {
		return null;
	}

	public default T selectStatsWorstDefExt(String nosaison) {
		return null;
	}
}
