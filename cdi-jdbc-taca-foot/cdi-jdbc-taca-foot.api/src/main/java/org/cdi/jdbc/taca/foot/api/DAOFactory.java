package org.cdi.jdbc.taca.foot.api;

public interface DAOFactory {

	public abstract DAO getMatchDAO();

	public default DAO getSaisonDAO() {
		return null;
	}

	public default DAO getTeamDAO() {
		return null;
	}

	public default DAO getClassementDAO() {
		return null;
	}
}
